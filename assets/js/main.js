var context = function () {
    var canvas = document.getElementById("canvas");

    var ContextManager = function () {
        this.clean_canvas = function () {
            // Clean Canvas
            console.log("Canvas cleaned");
            paper.project.activeLayer.removeChildren();
            paper.view.draw();
        }
    };

    var TrackGenerator = function (ctxManager) {
        scaleCanvasPointToRealPoint = function (point, canvasWidth, gridWidth) {
            var mid = Math.round(gridWidth / 2);
            var newX = (gridWidth * point.x / canvasWidth) - mid;
            var newY = (gridWidth * point.y / canvasWidth) - mid;

            return {
                'x': Math.round(newX * 10) / 10,
                'y': Math.round(newY * 10) / 10
            };
        };

        this.generateTrack = function (clone) {
            var paths = this.createPointsFromPath(clone);

            this.generateTrackPaths(paths);
        };

        this.generateTrackPaths = function (paths) {
            var itemCounter = 0;
            var modelName = $("#modelname").val();

            var static_model = $("#static_model").is(":checked");
            var static_model_tag = "";

            if (static_model === true) {
                static_model_tag = "<static>true</static>";
            }

            var template = '<include><uri>{model_name}</uri><pose>{x} {y} {z} 0 0 0</pose>' + static_model_tag + '<name>{model_unique_name}</name></include>';
            var preparedScript = '';

            // Clear old generated script
            $("[name=generated_script]").val("");

            paths.forEach(function (path) {
                path.points.forEach(function (point) {
                    var tmp = template;
                    tmp = tmp.replace('{x}', point.x);
                    tmp = tmp.replace('{y}', point.y);
                    tmp = tmp.replace('{z}', 0);
                    tmp = tmp.replace('{model_unique_name}', "item" + itemCounter);
                    tmp = tmp.replace('{model_name}', modelName);

                    preparedScript += tmp;
                    itemCounter++;
                });
            });

            preparedScript = vkbeautify.xml(preparedScript);

            $("[name=generated_script]").val(preparedScript);
        };

        this.createPointsFromPath = function (clone) {

            var gridSize = $("#gridsize").val();
            var trackWidth = $("#trackwidth").val();
            var modelStep = $("#modelstep").val();
            var modelName = $("#modelname").val();

            // Canvas
            var canvasWidth = Math.round(paper.view.viewSize.width);
            var canvasHeight = Math.round(paper.view.viewSize.height);

            // Get canvasStep by Propotions from Grid and Grid Step
            var canvasStep = Math.round(canvasWidth * modelStep / gridSize);

            var canvasTrackWidth = Math.round(canvasWidth * trackWidth / gridSize);

            var trackCloneScale = $("#trackclonescale").val();

            // Paths collection
            var paths = [], point;

            // Generate track
            console.log("Generate Track");
            console.log("Grid Size:", gridSize);
            console.log("Track Width:", trackWidth);
            console.log("Model Size:", modelStep);
            console.log("Model Name:", modelName);

            // Create Inner Paths
            if (clone && clone === true) {
                paper.project.activeLayer.children.forEach(function (child) {
                    if (child.className === "Path") {
                        var childClone = child.clone();
                        childClone.scale(trackCloneScale); //, new paper.Point(child.bounds.topCenter.x, childClone.position.y + canvasTrackWidth)); // , child.bounds.leftCenter
                        // childClone.position = new paper.Point(childClone.position.x, childClone.position.y + canvasTrackWidth)
                    }
                });
            }

            // Collect Points from Paths

            paper.project.activeLayer.children.forEach(function (child) {

                // Check if Path item
                if (child.className === "Path") {
                    paths.push({'path': child, 'points': []});

                    // Find Points by Path
                    for (var i = 0; i < child.length; i = i + canvasStep) {
                        // After calculation of the canvasStep, we need to take points after each canvasStep
                        point = scaleCanvasPointToRealPoint(child.getPointAt(i), canvasWidth, gridSize);
                        paths[paths.length - 1].points.push(point);
                    }
                }
            });

            return paths;
        };
    };

    // Constructor
    (function (ContextManager, TrackGenerator) {

        var ctxManager = new ContextManager();
        var trackGenerator = new TrackGenerator(ctxManager);

        $("#btn-clean-canvas").click(function (e) {
            e.preventDefault();
            ctxManager.clean_canvas();
        });

        $("#btn-clone-generate").click(function (e) {
            e.preventDefault();
            trackGenerator.generateTrack(true);
        });

        $("#btn-generate").click(function (e) {
            e.preventDefault();
            trackGenerator.generateTrack(false);
        });

    })(ContextManager, TrackGenerator);
};

$(document).ready(function () {
    context();
});