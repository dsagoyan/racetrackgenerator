# Racetrack Generator #

## Information ##
* Version: 0.0.1
* Author: Dmitry Sagoan
* Description: It's possible to draw a simple racetrack and then to copy generated SDF script into the Gazebo World File.

## Used Plugins and Libraries ##
1. Paperjs
2. svgparser
3. vkbeatify
4. bootstrap4/jquery

## TODO ##
1. Download generated Racetrack insteed of copying the generated content
2. Export/Import in JSON file
3. Possibility to edit already created track by importing JSON file
4. Solving issues with complex tracks
    a. Duplicated and scaled track are not always set in correct position.
